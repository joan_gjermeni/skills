<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$mod_strings = Array(
	'Skill Name' => 'Skill Name',
	'Skills' => 'Skills',
	'SINGLE_Skills' => 'Skill',
	'Skill No' => 'Skill No',

	'LBL_SKILLS_INFORMATION' => 'Skills Information',
	'LBL_DESCRIPTION_INFORMATION' => 'Description',

);

?>
